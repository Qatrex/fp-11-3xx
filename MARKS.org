
* 2016-03-03 (31 tests total)

| PR id | Surname/Name             | Mark | Percent | Failures | Errors | Failer            |                  |         |      |                               |
|-------+--------------------------+------+---------+----------+--------+-------------------+------------------+---------+------+-------------------------------|
|    87 | Abdullin      Dinar      |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    81 | Abramov       Vitaly     |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    43 | AdelB         ?          |    5 |   83.33 |        2 |      0 | n!!               |                  |         |      |                               |
|    67 | Aglyamov      Rafael     |    1 |   16.67 |        6 |      0 | primeSum          | isPrimeIsNotSlow | isPrime | n!!  |                               |
|    83 | Alexeev       Vladimir   |    5 |   83.33 |        2 |      0 | n!!               |                  |         |      |                               |
| /68,/ | Alsou         ?          |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    48 | Altynbaeva    Dilyara    |    5 |   83.33 |        2 |      0 | n!!               |                  |         |      |                               |
| /.34/ | AnastasiyaA   ?          |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|  /39/ | Aynur         ?          |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
| /50,/ | Baimurzin     Vladislav  |    4 |   66.67 |        1 |      2 | primeSum          | isPrime          |         |      |                               |
| /90,/ | bazinko       ?          |    5 |   83.33 |        2 |      0 | n!!               |                  |         |      |                               |
|    51 | Berkovich     Mira       |    4 |   66.67 |        1 |      0 | primeIsNotSlow    |                  |         |      |                               |
| /45./ | Boyko         Vladislav  |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    93 | Chegodaev     Denis      |    5 |   83.33 |        1 |      1 | n!!               |                  |         |      |                               |
|    42 | Dmitriev      Roman      |    3 |   50.00 |        3 |      0 | primeIsNotSlow    | n!!              |         |      |                               |
|    59 | Ermolaev      Konstantin |    4 |   66.67 |        2 |      3 | primeSum          | isPrime          |         |      | Внимательно изучайте тесты!   |
| /.17/ | Farrakhov     Aydar      |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|  /41/ | Fashetdinov   Rustam     |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
| /32/. | Fatkhullin    Rifat      |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    47 | Gazizov       Rim        |    1 |   16.67 |       10 |      0 | primeSum          | isPrimeIsNotSlow | isPrime | n!!  |                               |
|    20 | Gerasimov     Denis      |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    66 | gerasimova    ?          |    3 |   50.00 |        0 |      5 | primeSum          | isPrime          | 1hw2    |      |                               |
|    28 | Harisova      Alina      |    5 |   83.33 |        2 |      0 | n!!               |                  |         |      |                               |
|    44 | Himi???       Shoichi??? |    4 |   66.67 |        3 |      0 | isPrime           | n!!              |         |      |                               |
|     6 | Ignatiev      Maxim      |    3 |   50.00 |        2 |      1 | isPrimeIsNotSlow  | n!!              |         |      |                               |
|    55 | Kasymov       Ilham      |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|   /8/ | Kayumov       Kirill     |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|  /22/ | Kel           Alexander  |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|    24 | Khabetdinov   Rustem     |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|    16 | Khadeev       Ayrat      |    3 |   50.00 |        1 |      2 | isPrimeIsNotSlow  | n!!              |         |      |                               |
|    21 | lvelapel      ?          |    4 |   66.67 |       -- |     -- | ?                 |                  |         |      |                               |
|    31 | Mansurov      Eduard     |    4 |   66.67 |        6 |      0 | primeSum          | isPrime          |         |      |                               |
|    78 | Mariya        Z???       |    3 |   50.00 |        2 |      1 | primeSum          | isPrimeIsNotSlow |         |      | Внимательно изучайте тесты!   |
|    57 | Masalimova    Diana      |    0 |    0.00 |       20 |      6 | primeSum          | isPrime          | n!!     | 1hw2 |                               |
|    91 | Maxim         ?          |    3 |   50.00 |        4 |      0 | isPrimeIsNotSlow  | isPrime          |         |      |                               |
|    53 | Minushina     Rina       |    4 |   66.67 |        1 |      0 | primeIsNotSlow    |                  |         |      |                               |
|    89 | Mishin        Gena       |    3 |   50.00 |        3 |      3 | primeSum          | isPrime          | 1hw2    |      |                               |
| /33/. | Molokanov     Dmitry     |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    92 | Mulyukov      Ayrat      |    4 |   66.67 |        7 |      0 | n!!               | 1hw2             |         |      |                               |
|    84 | Nazmutdinova  Kamilya    |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    37 | Nizamov       Nurshat    |    4 |   66.67 |        1 |      0 | primeIsNotSlow    |                  |         |      |                               |
|    82 | Novikov       Stanislav  |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    64 | Oleggorru     ?          |    2 |   33.33 |        3 |      0 | isPrimeIsNotSlow  | isPrime          | n!!     |      |                               |
| /40./ | Osipova       Christina  |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    86 | Pavlova       Anna       |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    62 | Polyakh       Daniil     |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|    15 | railrem       ?          |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    88 | Rus           Ruslana    |    3 |   50.00 |        3 |      3 | isPrimeIsNotSlow  | isPrime          |         |      |                               |
|    60 | Rus           Ruslana    |    0 |    0.00 |       20 |      6 | primeSum          | isPrime          | n!!     | 1hw2 |                               |
|    23 | Rustavil      ?          |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
| /49,/ | Sabitov       Almaz      |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    35 | Safin         Vadim      |    3 |   50.00 |        5 |      0 | primeSum          | isPrime          | 1hw2    |      |                               |
|     7 | Sagidulin     Arthur     |    0 |    0.00 |       17 |      5 | primeSum          | isPrime          | n!!     | 1hw2 |                               |
|     9 | Salemgaraev   Rinat      |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    29 | Salemgaraev   Rinat      |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    52 | Sanbka        ?          |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    12 | Sayfeev       Ildar      |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    46 | Shaikhraziev  Nail       |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    38 | Stepanov      Sasha      |    0 |    0.00 |       20 |      6 | primeSum          | isPrime          | n!!     | 1hw2 |                               |
| /54./ | Suhanaewa     Sasha      |    0 |    0.00 |       20 |      6 | primeSum          | isPrime          | n!!     | 1hw2 |                               |
|    63 | Tagirov       Albert     |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    79 | Talipov       Zagit      |    2 |   33.33 |        1 |      4 | isPrimeIsNotSlow  | isPrime          | 1hw2    |      | Не изменяйте название модуля! |
|  /30/ | Talkambaev    Marsel     |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    27 | Tarasenko     Kate       |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|    58 | Tsimmerman    Arthur     |    3 |   50.00 |        3 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|    19 | Tsygankov     Ilya       |    5 |   83.33 |        2 |      0 | n!!               |                  |         |      |                               |
|    56 | Volodarskij   Bogdan     |    2 |   33.33 |        1 |      3 | isPrimeIsNotSlow  | n!!              | 1hw2    |      |                               |
| /77,/ | Yakbarov      Ruslan     |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
| /.26/ | Yakupov       Ilnur      |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|    14 | Yumaev        Ruzal      |      |    0.00 |       -- |     -- | AccessDenied      |                  |         |      |                               |
|    25 | Zagirova      Elina      |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|    18 | Zaripova      Renata     |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
#+TBLFM: $4=100*$3/6;%.2f

